package api;

import framework.Config;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeSuite;

public class BaseTest {

    public static final RequestSpecification REQUEST_SPEC_MOCK =
            new RequestSpecBuilder()
                    .setContentType("application/json")
                    .setBaseUri(String.format("%s:%s", Config.restServer1Host, Config.restServer1Port))
                    .build();

    @BeforeSuite
    public void addFilters() {
        RestAssured.filters(
                new RequestLoggingFilter(),
                new ResponseLoggingFilter()
        );
    }
}
