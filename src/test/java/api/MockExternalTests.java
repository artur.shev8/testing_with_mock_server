package api;


import framework.Config;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.specification.RequestSpecification;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.Arrays;

import static io.restassured.RestAssured.given;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

public class MockExternalTests extends BaseTest{

    private static ClientAndServer mockServer;

    @BeforeMethod
    public void startMockServer() {
        // before the tests make sure that System Under Tests is running
        mockServer = startClientAndServer(Integer.valueOf(Config.restServer2Port));

        // prepare mock response
        mockServer
                .when(
                        request().withMethod("POST")
                )
                .respond(
                        response().withStatusCode(200)
                );
    }

    @AfterMethod
    public void stopMockServer() {
        mockServer.stop();
    }

    @Test
    public void mock_external_test1() {
        // send request
        given()
                .spec(REQUEST_SPEC_MOCK)
                .body("{\"name\": \"test\"}")
                .when()
                .post("/")
                .then()
                .statusCode(200);

        // record check requests to mocked server
        HttpRequest[] mocked_requests = mockServer.retrieveRecordedRequests(request().withPath("/"));
        for (HttpRequest m_request : mocked_requests) {
            System.out.println("Recorded requests:\n" + m_request);
            System.out.println("---> request body:");
            System.out.println(m_request.getBodyAsJsonOrXmlString());
        }

    }
}
