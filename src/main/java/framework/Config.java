package framework;

public class Config {

    public static String restServer1Host = PropertyLoader.getProperty("restServer1Host");
    public static String restServer1Port = PropertyLoader.getProperty("restServer1Port");
    public static String restServer2Host = PropertyLoader.getProperty("restServer2Host");
    public static String restServer2Port = PropertyLoader.getProperty("restServer2Port");

}

