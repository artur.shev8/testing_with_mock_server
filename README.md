# Run proxy
(only for mac)
run in terminal:

    export LISTEN_ADDR=127.0.0.1:8080
    export DEFAULT_PROXYING_ADDR=http://destination_address
    export LOGGING=1

    chmod +x proxy

    ./proxy

if you need to see what proxy sends - run another proxy with LISTEN_ADDR as DEFAULT_PROXYING_ADDR from previous 

# Documentation
https://www.mock-server.com/#what-is-mockserver